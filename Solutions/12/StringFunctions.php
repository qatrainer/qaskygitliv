<?php

$message = 'DianeCalder';
echo $message[0]; //displays position 0 in quote
echo $message[5]; //displays position 5 in quote
echo "\n";

echo strlen($message); //displays length of string 
echo "\n";

echo strtolower("My name is Diane Calder!\n");
echo strtoupper("My name is Diane Calder!\n");
echo ucwords("My name is Diane Calder!\n");
echo "\n";

$quote = "Life is short, smile and be happy!\n";
$new_quote = str_replace("short", "long", $quote);
//$new_quote = str_replace("short", "long", "smile", "frown", "happy" , "miserable" , $quote);
echo $new_quote;
echo $quote;      // $quote is unchanged
echo"\n";

/*var_dump('Life is short, smile and be happy!');
var_dump(trim('Life is short, smile and be happy!'));
echo rtrim('Life is short, smile and be happy!',','); 
echo"\n";*/

$pieces = explode(' ', $quote); // this breaks apart $quote, splitting it at every space
print_r($pieces);
echo"\n";
print_r(array_unique(str_split ('Life is short, smile and be happy!')));
//array_unique() function removes duplicate values from an array, in this example it removes the letters already listed

$names = ["Diane", "Johnstone", "Robbie", "Hamish"];
$joined = implode(',', $names);   // this glues each piece together, putting a comma between them
echo "The people who live in my house are: $joined\n";
echo"\n";

// substr() is a function which gets part of string
$firstPart = substr($quote, 0, 13);  // start at 0 and get 13 characters
echo "$firstPart\n";

// with substr() and some other functions in php, you can supply a negative position:
$restOfQuote = substr($quote, 13); // from $quote, start at 13, copy till the end
echo "$restOfQuote\n";

// concatenation == joining
echo  "You have a lovely face " . $restOfQuote . "\n"; // the . operator joins strings together

// interpolation == substitution
$newMessage = 'Hi its lovely to see you';
echo "$newMessage, you're early!\n";  //recall: *double quotes* interpolate
echo '$message, you\'re early!';      //recall: *single quotes* do not
echo"\n";
echo"\n";

// formatting strings -- ADVANCED:

$height = 1.2345678;
echo "$height\n"; // gives us the all the information
printf($height); //printf will display content of variable
echo "\n";

printf("%.2f\n", $height); // just gives us two decimal places
printf("%.3f\n", $height); //gives 3 decimal places
echo "\n";
//sprintf("%.3f\n" , $height);//doesn't print anything here.

//short for string, print, formatted. 

//difference between printf and sprintf functions. printf will output a formatted string using placeholders, 
//sprintf will return the formatted string using a variable. The output is the same in the examples below. 

$name = 'Diane';
printf("Hello %s, How's it going?", $name);// The `%s` tells PHP to expect a string

$greeting = sprintf("Hello %s, How's it going?", $name);// Instead of outputting it directly, place it into a variable and then outputs using an echo.
echo $greeting;

//%d - integer, %s - string %f - float and rounds

