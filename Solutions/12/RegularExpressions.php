<?php

$quote = 'A smile is the best makeup a girl can wear!';
$isSmileFound = preg_match('/smile/', $quote);  //look for /smile/ in $quote

if($isSmileFound) {
  echo "'smile' was found in the quote\n";
} else {
    echo "smile' was not found in the quote";
}
  echo"\n";
  
//\d  means "a single digit character"
$bill = "You're shopping cart came to £145.99";
$containsPrice = preg_match('/£\d\d\d.\d\d/', $bill);
if($containsPrice) {
  echo "the bill contains the price of your shopping!\n";
}

$billWithoutPrice = "You're shopping was expensive!";
if(preg_match('/£\d\d\d.\d\d/', $billWithoutPrice)) {   // does not match now
  echo "the bill contains the price!\n";
} else {
  echo "the bill does not contain the price\n";         // so we get this!
}

// the regular expression for matching a simple date is:
// \d\d-\d\d-\d\d\d\d   that is, 2 digits - 2 digits - 4 digits

$datePattern = '/(\d\d)-(\d\d)-(\d\d\d\d)/';

$documents = [
  "Mr. Michael Burgess has an appointment on 01-10-2016\n",
  "Ms. Irene Adler has an appointment on 31-12-2016\n",
  "Mrs. Watson has an appointment on 01-12-2016\n"
];

foreach($documents as $document) {
  echo "\n\n";
  echo "ORIGINAL: $document\n";

  //1. MATCH  -- does the document contain a date?
  if(preg_match($datePattern, $document, $rememberedBits)) {
    echo "The document contains a date!\n\n";
  }
  //2. REMEMBER -- what bits are in the date?
  //$rememberedBits contains everything we wanted to remember: ie., the day, month, year
  echo "This is what we remembered:\n";
  print_r($rememberedBits);
  echo "\n\n";
  
  //3. REPLACE -- change the format of the date
  $newDocument = preg_replace($datePattern, '\1/\2/\3', $document);
  // \1/\2/\3 means
  //"put the thing we remembered 1st (ie. the day) first, then a /, then month 2nd..."
  echo "NEW: $newDocument\n";
}

/*number_format($number , decimalPlaces, decSymbol, thousandsSep);
$number = 19111977;
$english_format = number_format($number);*/
