# Atrait is simply a class that contains methods. This trait can be shared with many classes. All classes that use this trait can make use of the trait methods.

trait Greeting{
	
	public function sayHello($name){
		return 'Hello ' . $name;
	}

}

# Now we can use this trait in any class:

class Post{
	use Greeting;
}

class Page{
	use Greeting;
}



# Since we are using this trait in both classes above we now have access to the sayHello method in both instances: 


$post = new Post;
echo $post->sayHello('Meg');

$page = new Page;
echo $page->sayHello('Honey');