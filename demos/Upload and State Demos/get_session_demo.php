<?php
session_start();
?>
<!DOCTYPE html>
<html>
<body>

<?php
// Echo session variables that were set on previous page
echo "Favorite colour is " . $_SESSION["favcolour"] . ".<br>";
echo "Favorite animal is " . $_SESSION["favanimal"] . ".";

//or another way to  show all 
print_r($_SESSION);
?>

    
</body>
</html>